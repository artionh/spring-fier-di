package al.sda.fier.springfierdi.service;

public interface GreetingService {

    String sayGreeting();
}
