package al.sda.fier.springfierdi;

import al.sda.fier.springfierdi.controller.ConstructorInjectedController;
import al.sda.fier.springfierdi.controller.PropertyInjectedController;
import al.sda.fier.springfierdi.controller.SetterInjectedController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class SpringFierDiApplication {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(SpringFierDiApplication.class, args);

        System.out.println("------------ Property");
        PropertyInjectedController propertyInjectedController =
                (PropertyInjectedController) context.getBean("propertyInjectedController");
        System.out.println(propertyInjectedController.getGreeting());

        System.out.println("------------ Setter");
        SetterInjectedController setterInjectedController =
                (SetterInjectedController) context.getBean("setterInjectedController");
        System.out.println(setterInjectedController.getGreeting());

        System.out.println("------------ Constructor");
        ConstructorInjectedController constructorInjectedController =
                (ConstructorInjectedController) context.getBean("constructorInjectedController");
        System.out.println(constructorInjectedController.getGreeting());
    }
}
